# OpenML dataset: Capitol-Riot-Tweets

https://www.openml.org/d/43353

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A csv file with 80,000+ tweets from January 6th, 2021 -- the day of the capitol hill riots. Made using the Twitter Developer API + Tweepy.
Nowhere close to the size of the Parler data dumps, but anyone with NLP experience might be able to find something useful here.

tweets have mentions, hyperlinks, emojis, and punctuation removed. All text is converted to lowercase.
Some tweets have coordinates (if users had geotagging enabled).
Verified users have their usernames included
"user location" is the user's self reported location in their profile. Blank if it doesn't correspond to a US state (or DC)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43353) of an [OpenML dataset](https://www.openml.org/d/43353). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43353/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43353/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43353/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

